from django.urls import path

from personal_blog.views import (
    product_view,
    product_create_view,
    product_list_view,
    product_update_view,
    product_delete_view,
)

urlpatterns = [
    path('detail/<int:pk>/', product_detail_view, name='product_detail'),
    path('create/', product_create_view, name='product_create'),
    path('list/',product_list_view, name='product_list'),
    path('update/<int:pk>', product_update_view, name='product_update'),
    path('delete/<pk>', product_delete_view, name='product_delete'),
]