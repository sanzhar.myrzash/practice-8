from django.shortcuts import render, redirect, get_object_or_404
# from django.http import Http404
from ..models import Product
from ..forms import ProductForm



# Create your views here.
def index_view(request):
    return render(request, 'index.html')

def product_list_view(request):
    products = Product.objects.all()
    return render(request, 'product_list.html', context={'products': products})

def product_detail_view(request, *args, **kwargs):
    product = get_object_or_404(Product, pk=kwargs.get('pk'))
    return render(request, 'product_view.html', context={
        'product': product
    })

def product_create_view(request):
    if request.method == 'GET':
        form = ProductForm()
        return render(request, 'product_create.html', context={
            'form': form
        })
    elif request.method == 'POST':
        form = ProductForm(request.POST)
        if form.is_valid():
            new_product = Product.objects.create(
                    title = form.cleaned_data['title'],
                    body = form.cleaned_data['body'],
                    author = form.cleaned_data['author'],
                )
            return redirect('home_page')
        else:
            return render(request, 'product_create.html', context={
                'form': form})

def product_update_view(request, *args, **kwargs):
    product = get_object_or_404(Product, pk = kwargs.get('pk'))
    if request.method == 'GET':
        form = ProductForm(initial={
            'title': product.title,
            'body': product.body,
            'author': product.author,
        })
        return render(request, 'product_update.html', context={
            'form': form,
            'product': product
        })
    elif request.method == 'POST':
        form = ProductForm(data=request.POST)
        if form.is_valid():
            product.title = form.cleaned_data.get('title')
            product.body = form.cleaned_data.get('body')
            product.author = form.cleaned_data.get('author')
            product.save()
            return redirect('product_detail', pk=product.pk)
        else:
            return render(request, 'product_update.html', context={
                'form': form,
                'product': product
            })

def product_delete_view(request, *args, **kwargs):
    product = get_object_or_404(Product, pk=kwargs.get('pk'))
    if request.method == 'GET':
        return render(request, 'product_delete.html', context={
            'product': product
        })
    elif request.method == 'POST':
        product.delete()
        return redirect('home_page')