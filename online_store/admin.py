from django.contrib import admin
from .models import Product


class ProductAdmin(admin.ModelAdmin):
    list_display = ['title', 'author', 'created_at']
    list_filter = ['author']
    search_fields = ['title', 'body']
    fields = ['title', 'body', 'author']
    readonly_fields = ['created_at', 'updated_at']

admin.site.register(Product, ProductAdmin)
