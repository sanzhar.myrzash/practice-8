from django import forms
from django.forms import widgets

class ProductForm(forms.Form):
    name = forms.CharField(
        max_length=200,
        required=True,
        label="Name",
        widget=widgets.TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'Name',
        })
    )
    description = forms.CharField(
        max_length=3000,
        required=False,
        label="Description",
        widget=widgets.Textarea(attrs={
            'class': 'form-control',
            'placeholder': 'Some text...',
            'rows': 4,
        })
    )
    category = forms.CharField(
        max_length=100,
        required=True,
        label="Category",
        widget=widgets.TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'Category',
        })
    )
    price = forms.DecimalField(
        max_digits=10,
        decimal_places=2,
        required=True,
        label='Price',
        widget=widgets.NumberInput(attrs={
            'class': 'form-control',
            'placeholder': 'Price',
        })
    )
    image = forms.URLField(
        max_length=2000,
        required=True,
        label='Image',
        widget=widgets.TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'URL link',
        })
    )

class CategoryForm(forms.Form):
    title = forms.CharField(
        max_length=100,
        required=True,
        label='Category',
        widget=widgets.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Category',
                })
        )
