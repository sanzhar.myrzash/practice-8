from django.db import models

class Category(models.Model):
    title = models.CharField(max_length=100, null=False, blank=False, verbose_name="Category title")

    def __str__(self):
        return f"#{self.pk} - {self.title}"

class Product(models.Model):
    name = models.CharField(max_length=200, null=False, blank=False, verbose_name="Name")
    description = models.TextField(max_length=3000, null=False, blank=False, verbose_name="Description")
    category = models.ForeignKey(to=Category, on_delete=models.CASCADE, null=False, verbose_name='Category')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Create Date')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Update Date')
    price = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, verbose_name="Price")
    image = models.TextField(max_length=2000, null=False, blank=False, verbose_name="Product image")

def __str__(self):
    return "{}. {}".format(self.pk, self.name)